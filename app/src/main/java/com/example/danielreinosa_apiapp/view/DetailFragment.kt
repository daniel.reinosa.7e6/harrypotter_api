package com.example.danielreinosa_apiapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.danielreinosa_apiapp.R
import com.example.danielreinosa_apiapp.databinding.FragmentDetailBinding
import com.example.danielreinosa_apiapp.model.User
import com.example.danielreinosa_apiapp.model.UserAdapter
import com.example.danielreinosa_apiapp.viewmodel.DataviewModel


class DetailFragment : Fragment() {

    lateinit var binding: FragmentDetailBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProvider(requireActivity())[DataviewModel::class.java]

        viewModel.selectedCharacter.observe(viewLifecycleOwner) {
            binding.apodo.text = it.apodo
            binding.howartsHome.text = getString(R.string.casa_de_howarts,it.casaDeHogwarts)
            binding.personaje.text = it.personaje
            binding.nombreActor.text = getString(R.string.actor,it.interpretado_por)



            Glide.with(requireContext())
                .load(it.imagen)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.persImg)
        }
    }
}