package com.example.danielreinosa_apiapp.view

import com.example.danielreinosa_apiapp.model.UserAdapter
import android.os.Bundle
import android.text.Layout.Directions
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.danielreinosa_apiapp.Interface.MyOnClickListener
import com.example.danielreinosa_apiapp.R
import com.example.danielreinosa_apiapp.databinding.FragmentRecyclerViewBinding
import com.example.danielreinosa_apiapp.model.User
import com.example.danielreinosa_apiapp.model.json.DataItem
import com.example.danielreinosa_apiapp.viewmodel.DataviewModel
import androidx.navigation.fragment.findNavController


class RecyclerViewFragment : Fragment(), MyOnClickListener {

    lateinit var binding: FragmentRecyclerViewBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity()).get(DataviewModel::class.java)
        viewModel.data.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
        }
    }
    fun setUpRecyclerView(lista: ArrayList<DataItem>){
        val myAdapter = UserAdapter(lista, this)
        binding.recyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(context)
        }

    }


    override fun onClick(user: DataItem) {
        val viewModel = ViewModelProvider(requireActivity())[DataviewModel::class.java]
        viewModel.selectedCharacter.postValue(user)
        findNavController().navigate(R.id.action_recyclerViewFragment_to_detailFragment)


    }



}