package com.example.danielreinosa_apiapp.view

import com.example.danielreinosa_apiapp.Interface.APIInterface

class Repository {
    val apiInterface = APIInterface.create()

    suspend fun getTags() = apiInterface.getTags()
}