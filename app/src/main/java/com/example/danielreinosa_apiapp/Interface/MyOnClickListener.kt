package com.example.danielreinosa_apiapp.Interface

import com.example.danielreinosa_apiapp.model.json.DataItem

interface MyOnClickListener {
    fun onClick(user:DataItem)
}