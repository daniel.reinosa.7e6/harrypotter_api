package com.example.danielreinosa_apiapp.Interface

import com.example.danielreinosa_apiapp.model.json.Data
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface APIInterface {

    @GET("personajes")
    suspend fun getTags(): Response<Data>
        //Aquí posem les operacions GET,POST, PUT i DELETE vistes abans
        companion object {
            val BASE_URL = "https://harry-potter-api.onrender.com/"
            fun create(): APIInterface {
                val client = OkHttpClient.Builder().build()
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
                return retrofit.create(APIInterface::class.java)
            }
        }
    }

