package com.example.danielreinosa_apiapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.danielreinosa_apiapp.model.User
import com.example.danielreinosa_apiapp.model.json.Data
import com.example.danielreinosa_apiapp.model.json.DataItem
import com.example.danielreinosa_apiapp.view.Repository
import kotlinx.coroutines.*

class DataviewModel : ViewModel(){
    var data = MutableLiveData<Data>().apply { ArrayList<User>() }
    var repo = Repository()
    var selectedCharacter = MutableLiveData<DataItem>()

    init {
        fetchData()
    }
    fun fetchData(){
        CoroutineScope(Dispatchers.IO).launch{
            val response = repo.getTags()
            withContext(Dispatchers.Main){
                if (response.isSuccessful){
                    data.postValue(response.body())
                }
            }
        }
    }
    fun getAgent(uuid:String):DataItem?{
        var i = 0
        var agent: DataItem? = null
        while (i in data.value!!.indices&&agent==null){
            if(data.value!![i].id.toString() == uuid) agent = data.value!![i]
            i++
        }
        return agent
    }
}