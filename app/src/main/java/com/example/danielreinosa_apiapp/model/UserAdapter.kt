package com.example.danielreinosa_apiapp.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.danielreinosa_apiapp.Interface.MyOnClickListener
import com.example.danielreinosa_apiapp.R
import com.example.danielreinosa_apiapp.databinding.ItemUserBinding
import com.example.danielreinosa_apiapp.model.json.Data
import com.example.danielreinosa_apiapp.model.json.DataItem
import com.example.danielreinosa_apiapp.view.RecyclerViewFragment

class UserAdapter(private val users: java.util.ArrayList<DataItem>, private val listener: MyOnClickListener):
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemUserBinding.bind(view)
        fun setListener(user: DataItem){
            binding.root.setOnClickListener {
                listener.onClick(user)
            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]
        with(holder){
            setListener(user)
            binding.APIName.text = user.apodo
            binding.APIID.text = user.id.toString()
            Glide.with(context)
                .load(user.imagen)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.APIImage)
        }
    }

}
