package com.example.danielreinosa_apiapp.model

data class User(val id: Long, var name: String, var url: String)
