package com.example.danielreinosa_apiapp.model.json

data class DataItem(
    val apodo: String,
    val casaDeHogwarts: String,
    val estudianteDeHogwarts: Boolean,
    val hijos: List<String>,
    val id: Int,
    val imagen: String,
    val interpretado_por: String,
    val personaje: String
)